iOS版自定义CalloutView开源组件
========================
###概述
本开源代码中包含MAMapSMCalloutView和CustomAnnotationView用于实现可自定的CalloutView；
### 使用教程

- 在mapView：viewForAnnotation 中使用CustomAnnotationView作为MAAnnotationView；
```objc
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    CustomAnnotationView *annotationview = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"PinAnnotationsIdentifier"];
    
    if (annotationview == nil)
    {
        annotationview = [[CustomAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:@"PinAnnotationsIdentifier"];
    }
    annotationview.canShowCallout = YES;
    annotationview.image          = [UIImage imageNamed:@"poi_yellow"];
    return annotationview;
}
```
- 在mapView: didSelectAnnotationView:中 需要配置rightAccessoryView， calloutHeihgt,subviewClass,backgroundImage；
```objc
[mapView deselectAnnotation:view.annotation animated:NO];
_customCalloutView.rightAccessoryView = poiListView;
_customCalloutView.calloutHeight = 170;
_customCalloutView.subviewClass       = [UITableView class];
_customCalloutView.backgroundImage    = [UIImage imageNamed:@"map_bubble"];
[_customCalloutView presentCalloutFromRect:CGRectMake(view.bounds.origin.x,
                                                              view.bounds.origin.y,
                                                              view.bounds.size.width,
                                                              100)
                                            inView:view
                                 constrainedToView:self.view
                          permittedArrowDirections:MAMapSMCalloutArrowDirectionDown
                                          animated:YES];
```